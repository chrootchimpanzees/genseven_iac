source /etc/profile
export PS1="(chroot) ${PS1}"
mount /dev/vda1 /boot

# ebuild
emerge-webrsync
emerge --sync

emerge --verbose --update --deep --newuse @world

echo "Europe/Berlin" > /etc/timezone
emerge --config sys-libs/timezone-data

#locale options

echo "en_US.UTF-8 UTF-8"|cat - /etc/locale.gen > /tmp/out && mv /tmp/out /etc/locale.gen
locale-gen
eselect locale set 4

#Keyboard Options
#echo "KEYMAP=de" > /etc/vconsole.conf


# Environment aktualisieren
env-update && source /etc/profile && export PS1="(chroot) ${PS1}"

#Kernel configuration
emerge sys-kernel/linux-firmware
emerge sys-kernel/gentoo-sources
eselect kernel set 1

emerge sys-kernel/genkernel
echo "/dev/vda1	/boot	vfat	defaults	0 2" >> /etc/fstab

genkernel all

# optional:
# ls /boot/vmlinu* /boot/initramfs* > bootoptions

# System configure
echo "/dev/vda2	none	swap	sw  	    0 0" >> /etc/fstab
echo "/dev/vda3	/   	ext4	noatime	    0 1" >> /etc/fstab

echo "genmotown" > /etc/hostnames
sed -z -i 's/localhost/genmotown localhost/2}' /etc/hosts

systemd-machine-id-setup

echo "[Match]
Name=en*
 
[Network]
DHCP=yes" > /etc/systemd/network/50-dhcp.network

systemctl enable systemd-networkd.service
systemctl preset-all

echo "Please enter root password. Mit Sonderzeichen und Nummern und Buchstaben (bei mindestens 8 Zeichen)"
read -s root_password
export root_password

(
echo $root_password
echo $root_password
) | passwd


# Installing System Tools
emerge sys-apps/mlocate

emerge sys-fs/dosfstools

emerge net-misc/dhcpcd

# Bootloader konfigurieren

echo 'GRUB_PLATFORMS="efi-64"' >> /etc/portage/make.conf
emerge --verbose sys-boot/grub:2
grub-install --target=x86_64-efi --efi-directory=/boot
grub-mkconfig -o /boot/grub/grub.cfg