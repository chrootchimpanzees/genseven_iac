# Disks partitionieren
(
echo g
echo n
echo 
echo 
echo +256M
echo t
echo 1
echo n
echo 
echo 
echo +16G
echo t
echo 2
echo 19
echo n
echo 
echo 
echo 
echo w
)| fdisk /dev/vda

mkfs.vfat -F 32 /dev/vda1
mkfs.ext4 /dev/vda3

mkswap /dev/vda2
swapon /dev/vda2

mount /dev/vda3 /mnt/gentoo

cd /mnt/gentoo

# Zeit einstellen

ntpd -q -g


# Stage 3 Archiv (systemd) fetchen und entpacken

wget https://bouncer.gentoo.org/fetch/root/all/releases/amd64/autobuilds/20220720T223721Z/stage3-amd64-openrc-20220720T223721Z.tar.xz
tar xpvf stage3-*.tar.xz --xattrs-include='*.*' --numeric-owner


sed -z -i 's/-02/-march=skylake -02/}' /mnt/gentoo/etc/portage/make.conf
echo 'MAKEOPTS="-j4"' >> /mnt/gentoo/etc/portage/make.conf
echo 'ACCEPT_LICENSE="*"' >> /mnt/gentoo/etc/portage/make.conf


# /mnt/gentoo/etc/portage/make.conf ändern
# mirrors wählen
# use flags setzen




