# Gentoo Installation

1. **Image herunterladen**

    Diese Schritte sind optional
    1. Minimal Installation CD von https://www.gentoo.org/downloads/ herunterladen
    2. Von https://ftp.fau.de/gentoo/releases/amd64/autobuilds/current-install-amd64-minimal/ oder einem anderem Mirror die .DIGESTS file herunterladen
    3. Schlüssel herunterladen  
   
            gpg --keyserver hkps://keys.gentoo.org --recv-keys 0xBB572E0E2D182910

    4. 
            gpg --verify install-amd64-minimal-20141204.iso.DIGESTS
        Primary Key mit https://www.gentoo.org/downloads/signatures/ abgleichen (releases)
    5. SHA512 Prüfsumme anzeigen lassen (ist die erste die nach diesem Command kommt)

            grep -A 1 -i sha512 install-amd64-minimal-20141204.iso.DIGESTS

    6. Die Prüfsumme mit der Prüfsumme der ISO Datei vergleichen

            sha512sum install-amd64-minimal-20141204.iso
        
2. **Netzwerk**

    Mit ifconfig und ping prüfen ob eine Netzwerkverbindung bereits vorhanden ist.

3. **Disks**

    1. Herausfinden ob die das Motherboard UEFI oder BIOS benutzt. Falls der Ordner /sys/firmware/efi existiert benutzt es UEFI und man kann mit dieser Anleitung weitermachen.

            fdisk -l 

        Zum einzeigen der Disks und Partitionen.

        Es müssen 3 Partitionen erstellt werden. ESP, Swap und Root

            fdisk /dev/sda

        g drücken um Disklabel type gpt zu erstellen. Mit d und 1-n alle Partitionen löschen. p zum Anzeigen von Informationen

    2. ESP erstellen:

        -> n -> Enter -> Enter (First Sector bei 2048) -> +256M (Last Sector)

        -> t -> 1 (als Efi markieren)

    3. SWAP erstellen:

        -> n -> Enter -> Enter -> +16G (2 * RAM)

        -> t -> 2 -> 19
    
    4. Root erstellen

        -> n -> Enter -> Enter -> Enter

    5. w zum Speichern

    6. Zu entsprechenden Dateisystem formatieren:
            
            mkfs.vfat -F 32 /dev/sda1
            mkfs.ext4 /dev/sda3

    7. Aktivieren der Swap Partition

            mkswap /dev/sda2
            swapon /dev/sda2

    8. Root Verzeichnis mounten.
   
            mount /dev/sda3 /mnt/gentoo

4. **Installation Stage-Tar-Archiv**

    1. Mit date prüfen, ob das Datum stimmt. (Achtung UTC Zeit beibehalten). Mit ntpd -q -g aktualisieren
    2. (optional) /etc/ntp.conf ändern in eine Zeile mit : server ntp01.urz.uni-heidelberg.de 
    3. links benutzen um staded-tarball mit systemd herunterzuladen

            links https://www.gentoo.org/downloads/mirrors/
    Zu de navigieren dann zu releases/amd64/autobuilds/"PLACEHOLDER->LATESTDATE"
    ..amd64-systemd...tar.xz herunterladen

    4. Auspacken:

            tar xpvf stage3-*.tar.xz --xattrs-include='*.*' --numeric-owner

    5. Compile Optionen einstellen:

            vi /mnt/gentoo/etc/portage/make.conf

        Comet Lake safe flag oben eingeben

        Makeopts eingeben.

        Accept License auf all einstellen.

5.  Installationn Gentoo Basissystem

    1. Mirror ändern.

            mirrorselect -i -o >> /mnt/gentoo/etc/portage/make.conf

    2. Repo Sachen machen

            mkdir --parents /mnt/gentoo/etc/portage/repos.conf
            cp /mnt/gentoo/usr/share/portage/config/repos.conf /mnt/gentoo/etc/portage/repos.conf/gentoo.conf

    3. DNS Info kopieren

            cp --dereference /etc/resolv.conf /mnt/gentoo/etc/

    4. Notwendige Dateisysteme einhängen

            mount --types proc /proc /mnt/gentoo/proc 
            mount --rbind /sys /mnt/gentoo/sys 
            mount --make-rslave /mnt/gentoo/sys 
            mount --rbind /dev /mnt/gentoo/dev 
            mount --make-rslave /mnt/gentoo/dev 
            mount --bind /run /mnt/gentoo/run 
            mount --make-slave /mnt/gentoo/run 

    5. Betreten der neuen Umgebung

            chroot /mnt/gentoo /bin/bash
            source /etc/profile
            export PS1="(chroot) ${PS1}"

    6. Mount Boot Partition

            mount /dev/sda1 /boot

    7. Ein Gentoo ebuild Repository Snapshot aus dem Web installieren

            emerge-webrsync

        Gentoo ebuild Repository aktualisieren

            emerge --sync

    8. eprofile wählen (vanilla)

            eselect profile set 10 

    9. WELT ERSTELLEN

            emerge --ask --verbose --update --deep --newuse @world
